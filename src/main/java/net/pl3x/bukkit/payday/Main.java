package net.pl3x.bukkit.payday;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.payday.configuration.Config;
import net.pl3x.bukkit.payday.hook.Vault;
import net.pl3x.bukkit.payday.task.Payday;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	private static Main instance;

	public static Main getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {
		instance = this;

		saveDefaultConfig();

		boolean disabled = false;
		List<String> errors = new ArrayList<String>();

		if (!Bukkit.getPluginManager().isPluginEnabled("Vault")) {
			errors.add("&4#       This plugin requires the &eVault &4plugin to be installed!        #");
			disabled = true;
		}
		if (disabled) {
			Logger.log("&4#######################################################################");
			for (String error : errors) {
				Logger.log(error);
			}
			Logger.log("&4#                                                                     #");
			Logger.log("&4# To prevent server crashes and other undesired behavior this plugin  #");
			Logger.log("&4#   is disabling itself from running. Please remove the plugin jar    #");
			Logger.log("&4#   from your plugins directory to free up the registered commands.   #");
			Logger.log("&4#######################################################################");
			return;
		}

		Vault.setupEconomy();

		int ticks = Config.INTERVAL.getInt() * 20;
		new Payday().runTaskTimer(this, ticks, ticks);

		Logger.log(getName() + " v" + getDescription().getVersion() + " enabled!");
	}

	@Override
	public void onDisable() {
		Logger.log(getName() + " disabled.");
	}
}
