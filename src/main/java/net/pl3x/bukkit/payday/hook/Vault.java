package net.pl3x.bukkit.payday.hook;

import java.util.UUID;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.bukkit.payday.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Vault {
	private static Economy economy;

	public static Economy getEconomy() {
		return economy;
	}

	public static boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}
		return (economy != null);
	}

	public static String format(double amount) {
		return economy.format(amount);
	}

	public static void depositPlayer(UUID uuid, double amount) throws Exception {
		EconomyResponse response = getEconomy().depositPlayer(Bukkit.getOfflinePlayer(uuid), amount);
		if (!response.transactionSuccess()) {
			throw new Exception(Lang.VAULT_DEPOSIT_ERROR.get().replace("{error}", response.errorMessage));
		}
	}
}
