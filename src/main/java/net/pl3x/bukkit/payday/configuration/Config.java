package net.pl3x.bukkit.payday.configuration;

import net.pl3x.bukkit.payday.Main;

public enum Config {
	COLOR_LOGS(true),
	DEBUG_MODE(false),
	LANGUAGE_FILE("lang-en.yml"),
	AMOUNT(100.0),
	INTERVAL(3600);

	private Main plugin;
	private final Object def;

	private Config(Object def) {
		this.plugin = Main.getInstance();
		this.def = def;
	}

	public String getKey() {
		return name().toLowerCase().replace("_", "-");
	}

	public void set(Object value) {
		plugin.getConfig().set(getKey(), value);
	}

	public int getInt() {
		return plugin.getConfig().getInt(getKey(), (Integer) def);
	}

	public double getDouble() {
		return plugin.getConfig().getDouble(getKey(), (Double) def);
	}

	public String getString() {
		return plugin.getConfig().getString(getKey(), (String) def);
	}

	public boolean getBoolean() {
		return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
	}
}
