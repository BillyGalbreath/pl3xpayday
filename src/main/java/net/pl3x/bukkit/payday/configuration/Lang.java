package net.pl3x.bukkit.payday.configuration;

import java.io.File;

import net.pl3x.bukkit.payday.Logger;
import net.pl3x.bukkit.payday.Main;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Lang {
	PAYDAY_AMOUNT_CANNOT_BE_ZERO_OR_LOWER("&4Payday amount cannot be 0 or lower! (Current: &7{amount}&4)"),
	VAULT_DEPOSIT_ERROR("&4An error has occurred trying to deposit to account! {error}"),
	PAYDAY_RECEIVED("&e[&aPayday&e] &dYou have recieved &7{amount} &dfor payday!");

	private Main plugin;
	private String def;

	private File configFile;
	private FileConfiguration config;

	private Lang(String def) {
		this.plugin = Main.getInstance();
		this.def = def;
		configFile = new File(plugin.getDataFolder(), Config.LANGUAGE_FILE.getString());
		saveDefault();
		reload();
	}

	public void reload() {
		config = YamlConfiguration.loadConfiguration(configFile);
	}

	public void saveDefault() {
		if (!configFile.exists()) {
			plugin.saveResource(Config.LANGUAGE_FILE.getString(), false);
		}
	}

	public String getKey() {
		return name().toLowerCase().replace("_", "-");
	}

	public String get() {
		String value = config.getString(getKey(), def);
		if (value == null) {
			Logger.log("Missing lang data: " + getKey());
			value = "&c[missing lang data]";
		}
		return ChatColor.translateAlternateColorCodes('&', value);
	}
}
