package net.pl3x.bukkit.payday.task;

import net.pl3x.bukkit.payday.Logger;
import net.pl3x.bukkit.payday.configuration.Config;
import net.pl3x.bukkit.payday.configuration.Lang;
import net.pl3x.bukkit.payday.hook.Vault;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Payday extends BukkitRunnable {

	@Override
	public void run() {
		Double amount = Config.AMOUNT.getDouble();
		if (amount <= 0) {
			Logger.log(Lang.PAYDAY_AMOUNT_CANNOT_BE_ZERO_OR_LOWER.get().replace("{amount}", amount.toString()));
			return;
		}

		String message = Lang.PAYDAY_RECEIVED.get().replace("{amount}", amount.toString());

		for (Player player : Bukkit.getOnlinePlayers()) {
			try {
				Vault.depositPlayer(player.getUniqueId(), amount);
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			} catch (Exception e) {
				Logger.log(e.getMessage());
			}
		}
	}

}
