package net.pl3x.bukkit.payday;

import java.util.logging.Level;

import net.pl3x.bukkit.payday.configuration.Config;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {
	public static void log(String msg) {
		Main plugin = Main.getInstance();
		if (plugin.getConfig().getBoolean("color-logs", true)) {
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3[&d" + plugin.getName() + "&3]&r " + msg));
			return;
		}
		Bukkit.getLogger().log(Level.INFO, "[" + plugin.getName() + "] " + ChatColor.stripColor(msg));
	}

	public static void debug(String msg) {
		if (!Config.DEBUG_MODE.getBoolean()) {
			return;
		}
		log("&7[&eDEBUG&7]&r" + msg);
	}
}
